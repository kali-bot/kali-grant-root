Source: kali-grant-root
Section: misc
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Build-Depends: debhelper-compat (= 12)
Standards-Version: 4.4.1
Vcs-Git: https://gitlab.com/kalilinux/packages/kali-grant-root.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/kali-grant-root

Package: kali-grant-root
Architecture: all
Depends: sudo, policykit-1, adduser, ${misc:Depends}
Description: Configuration controlling privilege escalation to root
 Penetration tester often use applications that require root privileges
 to perform their work. The default configuration requires the user to
 input his password to be granted root rights.
 .
 With this package installed, you can simply add the user to the
 "kali-trusted" group and it will automatically benefit from
 password-less privilege escalation. This is a convenience feature
 but also a security risk, use with caution and make sure that you
 don't leave your computer unattented!
 .
 You can quickly enable/disable this feature with "dpkg-reconfigure
 kali-grant-root". It will populate the "kali-trusted" groups with all the
 members of the "sudo" group.
